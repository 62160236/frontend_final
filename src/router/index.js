import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from '../views/Main.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: ' Main',
    component: Main
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Registerpage.vue')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Loginpage.vue')
  },
  {
    path: '/mainmenu',
    name: 'Mainmenu',
    component: () => import('../views/Mainmenu.vue')
  },
  {
    path: '/mainpage',
    name: 'Mainpage',
    component: () => import('../views/Mainpage.vue')
  },
  {
    path: '/forget',
    name: 'Forget',
    component: () => import('../views/Forgetpage.vue')
  },
  {
    path: '/home',
    name: 'Home',
    component: () => import('../views/Home.vue')
  },
  {
    path: '/mainmenu',
    name: 'Mainmenu',
    component: () => import('../views/Mainmenu.vue')
  },
  {
    path: '/forminput1',
    name: 'FormInput1',
    component: () => import('../views/FormInput1.vue')
  },
  {
    path: '/calendar',
    name: 'Calendar',
    component: () => import('../views/Calendar.vue')
  },
  {
    path: '/roomtable',
    name: 'RoomTable',
    component: () => import('../views/product/RoomTable.vue')
  },
  {
    path: '/roomform',
    name: 'RoomForm',
    component: () => import('../views/product/RoomForm.vue')
  },
  {
    path: '/buildingtable',
    name: 'BuildingTable',
    component: () => import('../views/product/BuildingTable.vue')
  },
  {
    path: '/buildingform',
    name: 'BuildingForm',
    component: () => import('../views/product/BuildingForm.vue')
  },
  {
    path: '/userform',
    name: 'UserForm',
    component: () => import('../views/product/UserForm.vue')
  },
  {
    path: '/usertable',
    name: 'UserTable',
    component: () => import('../views/product/UserTable.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
