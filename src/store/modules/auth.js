import { AUTH_LOGIN, AUTH_LOGOUT } from '../mutation-types'
import router from '../../router'
export default {
  namespaced: true,
  state: () => ({
    user: null
  }),
  mutations: {
    [AUTH_LOGIN] (state, payload) {
      state.user = payload
    },
    [AUTH_LOGOUT] (state) {
      state.user = null
    }
  },
  actions: {
    login ({ commit }, payload) {
      console.log(payload)
      // if correct
      const user = { name: 'Administrator', email: 'admin@user.com' }
      router.push('/mainmenu')
      commit(AUTH_LOGIN, user)
    },
    logout ({ commit }) {
      commit(AUTH_LOGOUT)
    }
  },
  getters: {
    isLogin (state, getters) {
      return state.user != null
    }
  }
// eslint-disable-next-line eol-last
}